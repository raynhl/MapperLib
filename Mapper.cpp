/*
  Mapper.h - Library to map Cartesian coordinates to GPS Coordinates.
  Created by Ray Ng (nhlhanlim93@gmail.com), September 29, 2016.
  Released into the public domain.
*/

//Remove when release
//#include <iostream>
//using namespace std;
  
#include "Arduino.h"
#include "Mapper.h"

// Construct a Mapper object based on a defined reference point

// Step 1: Get a reference point from <http://www.maps.ie/>
// Step 2: Go to <http://www.apsalin.com/convert-geodetic-to-cartesian.aspx> to convert to ECEF
// eg: xyz for Petronas Twin Tower is (-1292778.69951722,6235923.25115904,348991.679260659)

// @param   double x        The x coordinate in ECEF reference
// @param   double y        The y coordinate in ECEF reference
// @param   double z        The z coordinate in ECEF reference

// @return  Mapper object

Mapper::Mapper(double x, double y, double z)
{
  x_ref = x;
  y_ref = y;
  z_ref = z;
}

//  Returns a displaced latitude based on the reference point defined in Mapper object
//  Can input values from Pozyx reading directly into parameters.

// @param   double x        delta x in mm from the reference point
// @param   double y        delta y in mm from the reference point
// @param   double z        delta z in mm from the reference point

double Mapper::getRelativeLatitude(double x, double y, double z) {
  
  double a = 6378137;
  double f_inv = 298.257223563;

  x = (double) x/1000 + x_ref;
  y = (double) y/1000 + y_ref;
  z = (double) z/1000 + z_ref;

  double p;
  double r;
  double e_2;
  double f;
  double u;
  double lat;
  
  p = sqrt(x*x + y*y);
  r = sqrt(p*p + z*z);
  f = 1/f_inv;
  e_2 = f*(2-f);

  u = atan((z/p)*((1-f)+e_2*a/r));

  lat = atan((z*(1-f)+e_2*a*pow(sin(u),3))/((1-f)*(p-e_2*a*pow(cos(u),3))));
  lat = lat * 180/M_PI;
  return lat;
}

//  Returns a displaced longitude based on the reference point defined in Mapper object
//  Can input values from Pozyx reading directly into parameters.

// @param   double x        delta x in mm from the reference point
// @param   double y        delta y in mm from the reference point
// @param   double z        delta z in mm from the reference point
double Mapper::getRelativeLongitude(double x, double y, double z) {
  
  double a = 6378137;
  double f_inv = 298.257223563;

  x = (double) x/1000 + x_ref;
  y = (double) y/1000 + y_ref;
  z = (double) z/1000 + z_ref;

  double p;
  double r;
  double e_2;
  double f;
  double u;
  double lon;

  p = sqrt(x*x + y*y);
  r = sqrt(p*p + z*z);
  f = 1/f_inv;
  e_2 = f*(2-f);

  u = atan((z/p)*((1-f)+e_2*a/r));

  lon = atan(y/x);
  lon = 180 + lon * 180/M_PI;

  return lon;
}

//  Returns a displaced altitude based on the reference point defined in Mapper object
//  Can input values from Pozyx reading directly into parameters.

// @param   double x        delta x in mm from the reference point
// @param   double y        delta y in mm from the reference point
// @param   double z        delta z in mm from the reference point
double Mapper::getRelativeAltitude(double x, double y, double z) {
  
  double a = 6378137;
  double f_inv = 298.257223563;

  x = (double) x/1000 + x_ref;
  y = (double) y/1000 + y_ref;
  z = (double) z/1000 + z_ref;

  double p;
  double r;
  double e_2;
  double f;
  double u;
  double lat;
  double alt;

  p = sqrt(x*x + y*y);
  r = sqrt(p*p + z*z);
  f = 1/f_inv;
  e_2 = f*(2-f);

  u = atan((z/p)*((1-f)+e_2*a/r));

  lat = atan((z*(1-f)+e_2*a*pow(sin(u),3))/((1-f)*(p-e_2*a*pow(cos(u),3))));

  alt = p*cos(lat) + z*sin(lat) - a*sqrt(1-e_2*sin(lat)*sin(lat));
  return alt;
}