/*
  Mapper.h - Library to map Cartesian coordinates to GPS Coordinates.
  Created by Ray Ng (nhlhanlim93@gmail.com), September 29, 2016.
  Released into the public domain.
*/

#ifndef Mapper_h
#define Mapper_h

#include "Arduino.h"
#include <math.h>

class Mapper
{
  public:

  	// Construct a Mapper object based on a defined reference point

  	// Step 1: Get a reference point from <http://www.maps.ie/>
  	// Step 2: Go to <http://www.apsalin.com/convert-geodetic-to-cartesian.aspx> to convert to ECEF
  	// eg: xyz for Petronas Twin Tower is (-1292778.69951722,6235923.25115904,348991.679260659)

  	// @param		double x        The x coordinate in ECEF reference
  	// @param		double y        The y coordinate in ECEF reference
  	// @param		double z        The z coordinate in ECEF reference

  	// @return	Mapper object
    Mapper(double x, double y, double z);

   	//  Returns a displaced latitude based on the reference point defined in Mapper object
    //	Can input values from Pozyx reading directly into parameters.

   	// @param		double x        delta x in mm from the reference point
 	  // @param		double y        delta y in mm from the reference point
  	// @param		double z        delta z in mm from the reference point
    double getRelativeLatitude(double x, double y, double z);

    //  Returns a displaced longitude based on the reference point defined in Mapper object
    //	Can input values from Pozyx reading directly into parameters.

   	// @param		double x        delta x in mm from the reference point
 	// @param		double y        delta y in mm from the reference point
  	// @param		double z        delta z in mm from the reference point
    double getRelativeLongitude(double x, double y, double z);

    //  Returns a displaced altitude based on the reference point defined in Mapper object
    //	Can input values from Pozyx reading directly into parameters.

   	// @param		double x        delta x in mm from the reference point
 	// @param		double y        delta y in mm from the reference point
  	// @param		double z        delta z in mm from the reference point
    double getRelativeAltitude(double x, double y, double z);

  private:
  	double x_ref;
  	double y_ref;
  	double z_ref;
};

#endif